
<h2 class="center">Błąd 404</h2>
<h3 class="center">Strona nieznaleziona</h3>
<p class="center">Strona <em><?=URL::site(Request::detect_uri(), TRUE)?></em> nie została znaleziona.</p>