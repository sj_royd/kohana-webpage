<?php

defined('SYSPATH') or die('No direct script access.');

class Kohana extends Kohana_Core {

	/**
	 * Changes the currently enabled modules. Module paths may be relative
	 * or absolute, but must point to a directory:
	 *
	 *     Kohana::modules(array('modules/foo', MODPATH.'bar'));
	 *
	 * @param   array  list of module paths
	 * @return  array  enabled modules
	 */
	public static function modules(array $modules = NULL) {
		if ($modules === NULL) {
			// Not changing modules, just return the current set
			return Kohana::$_modules;
		}

		// Start a new list of include paths, APPPATH first
		$paths = [APPPATH];

		foreach ($modules as $name => $path) {
			if (is_file($path . '.phar')) {
				// Add phar-version of the the module to include paths
				$paths[] = $modules[$name] = 'phar://' . realpath($path . '.phar') . DIRECTORY_SEPARATOR;
			} elseif (strstr($path, 'phar://')) {
				$paths[] = $modules[$name] = $path . DIRECTORY_SEPARATOR;
			} elseif (is_dir($path)) {
				// Add the module to include paths
				$paths[] = $modules[$name] = realpath($path) . DIRECTORY_SEPARATOR;
			} else {
				// This module is invalid, remove it
				unset($modules[$name]);
			}
		}

		// Finish the include paths by adding SYSPATH
		$paths[] = SYSPATH;

		// Set the new include paths
		Kohana::$_paths = $paths;

		// Set the current module list
		Kohana::$_modules = $modules;

		foreach (Kohana::$_modules as $path) {
			$init = $path . 'init' . EXT;

			if (is_file($init)) {
				// Include the module initialization file once
				require_once $init;
			}
		}

		return Kohana::$_modules;
	}

	/**
	 *
	 * @param string $scriptName
	 * @return string
	 */
	public static function getLangJs($scriptName) {
		if (($conf = Kohana::$config->load('multilang')->get('languages'))) {
			if (isset($conf[I18n::lang()], $conf[I18n::lang()][$scriptName])) {
				return $conf[I18n::lang()][$scriptName];
			}
		}
		return null;
	}

}

// End Kohana