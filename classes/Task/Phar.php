<?php

class Task_Phar extends Minion_Task {

	protected $_options = array(
        'module' => null,
		'system' => null,
		'application' => null,
		'all' => null,
    );

	protected function _execute(array $params) {

		if($params['application']){
			$this->archiveApplication();
		}

		if($params['system']){
			$this->archiveSystem();
		}

		if($params['module']){
			$this->archiveModule($params['module']);
		}

		if($params['all']){
			$this->archiveAll();
		}

	}

	/**
	 * Archiwizuje katalog aplikacji
	 */
	protected function archiveApplication(){
		$this->copyKohana();

		$archive_name = DOCROOT.'application.phar';
		$dir_path = APPPATH;

		$archive = new Phar($archive_name);
		$archive->startBuffering();
		$archive->buildFromDirectory($dir_path);
		$archive->stopBuffering();
		$archive->compressFiles(Phar::GZ);
		$archive->setSignatureAlgorithm(Phar::SHA1);
	}

	/**
	 * Archiwizuje katalog frameworka
	 */
	protected function archiveSystem(){
		$archive_name = SYSPATH.'../system.phar';
		$dir_path = SYSPATH;

		$archive = new Phar($archive_name);
		$archive->startBuffering();
		$archive->buildFromDirectory($dir_path);
		$archive->stopBuffering();
		$archive->compressFiles(Phar::GZ);
		$archive->setSignatureAlgorithm(Phar::SHA1);
	}

	/**
	 * Archiwizuje wybrany moduł
	 * @param string $moduleName Nazwa modułu
	 * @throws Kohana_Exception
	 */
	protected function archiveModule($moduleName){
		$modules = Kohana::modules();
		$modulePath = realpath(preg_replace(['~^phar:\/\/~', '~\.phar~'], ['', ''], $modules[$moduleName]));
		if (is_dir($modulePath)){
			$this->copyKohana();

			$archive_name = "{$modulePath}.phar";
			$dir_path = $modulePath;

			$archive = new Phar($archive_name);
			$archive->startBuffering();
			$archive->buildFromDirectory($dir_path);
			$archive->stopBuffering();
			$archive->compressFiles(Phar::GZ);
			$archive->setSignatureAlgorithm(Phar::SHA1);
		} else {
			throw new Kohana_Exception('Attempted to load an invalid or missing module \':module\' at \':path\'', array(
				':module' => $moduleName,
				':path'   => Debug::path($modules[$moduleName]),
			));
		}
	}

	/**
	 * Kopiuje klasę Kohany pozwalając na używanie plików PHAR
	 */
	private function copyKohana(){
		if(!strstr(APPPATH, 'phar')){
			copy(dirname(__FILE__).'/../Kohana.php', APPPATH.'classes/Kohana.php');
		}
	}

	/**
	 * Archiwizuje aplikację wraz z frameworkiem oraz modułami
	 * @throws Kohana_Exception
	 */
	protected function archiveAll(){
		$this->copyKohana();

		$archive_name = DOCROOT.'compact_application.phar';

		$archive = new Phar($archive_name);
		$archive->startBuffering();
		$archive->buildFromIterator(
			new RecursiveIteratorIterator(new RecursiveDirectoryIterator(APPPATH, FilesystemIterator::SKIP_DOTS)),
			dirname(APPPATH)
		);
		$archive->buildFromIterator(
			new RecursiveIteratorIterator(new RecursiveDirectoryIterator(SYSPATH, FilesystemIterator::SKIP_DOTS)),
			dirname(SYSPATH)
		);

		$modules = Kohana::modules();
		foreach($modules as $module){
			$modulePath = realpath(preg_replace(['~^phar:\/\/~', '~\.phar~'], ['', ''], $module));
			if (is_dir($modulePath)){
				$archive->buildFromIterator(
					new RecursiveIteratorIterator(new RecursiveDirectoryIterator($modulePath, FilesystemIterator::SKIP_DOTS)),
					dirname(realpath($modulePath.'/../'))
				);
			}
		}

		if(($userDirs = Kohana::$config->load('compact_phar')->get('user_dirs'))){
			foreach($userDirs as $dir){
				$archive->buildFromIterator(
					new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir, FilesystemIterator::SKIP_DOTS)),
					dirname(realpath($dir))
				);
			}
		}

		$archive->stopBuffering();
		$archive->compressFiles(Phar::GZ);
		$archive->setSignatureAlgorithm(Phar::SHA1);
	}

}
