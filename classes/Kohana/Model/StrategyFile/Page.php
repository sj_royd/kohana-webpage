<?php

class Kohana_Model_StrategyFile_Page implements Model_iPage {

	public function getContentByUrl($url) {
		$item = Lib_Menu::instance('menu')->getByLink($url);
		$content = '';
		if($item){
			$templatesPath = Lib_Menu::instance('menu')->getTemplatesPath($item);
			foreach($item['templates'] as $template){
				try {
					$content .= View::factory($templatesPath.$template)->render();
				} catch (Exception $ex) {
					$content .= View::factory('errors/404-section', ['template'=>$templatesPath.$template])->render();
				}
			}
		} else {
			$content = View::factory('errors/404-site')->render();
		}
		return $content;
	}


}
