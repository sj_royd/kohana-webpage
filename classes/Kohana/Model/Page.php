<?php

class Kohana_Model_Page extends Model {

	private $model;

	public function __construct() {
		$config = Kohana::$config->load('webpage');
		$strategy = ucfirst($config->get('strategy'));
		$modelName = "Model_Strategy{$strategy}_Page";
		$this->model = new $modelName;
	}

	public function __call($method, $arguments) {
//		if(method_exists($this->model, $method)){
			return call_user_func_array([$this->model, $method], $arguments);
//		} else {
//			throw Exce
//		}
	}

}
