<?php

class Kohana_Lib_Menu {

	protected $menuConfig;

	protected $index;
	protected $menuHtml = '';

	protected $menuFlatByUrl = [];

	private static $instances = [];

	/**
	 *
	 * @param string $index
	 * @return Lib_Menu
	 */
	public static function instance($index){
		if(!isset(self::$instances[$index])){
			self::$instances[$index] = new self($index);
		}

		return self::$instances[$index];
	}

	/**
	 *
	 * @param type $index
	 */
	protected function __construct($index){
		$this->index = $index;
		$this->menuConfig = Kohana::$config->load('menu');
		$menu = $this->menuConfig->get($this->index);
		$this->flatByUrl($menu);
		$this->menuHtml = $this->prepareMenu($menu);
	}

	/**
	 *
	 * @param array $menu
	 */
	protected function flatByUrl($menu, $parent = null){
		foreach($menu as $item){
			if(isset($item['link'])){
				$children = Arr::get($item, 'children') ?: null;
				unset($item['children']);
				$item['parent'] = $parent;
				if($children){
					$this->flatByUrl($children, $item['link']);
					foreach($children as $child){
						$item['children'][] = $child['link'];
					}
				} else {
					$item['children'] = null;
				}
				$this->menuFlatByUrl[$item['link']] = $item;
			}
		}
	}

	/**
	 *
	 * @param array $tree
	 * @param mixed $parent
	 * @param int $depth
	 */
	protected function prepareMenu(&$tree, &$parent = null, $depth = 1){
		$menu = "<ul class=\"depth{$depth}\">";
		foreach($tree as &$leaf){
			if(!$this->checkAcl($leaf)){ continue; }
			if(in_array('exclude', $leaf)){ continue; }
			$this->checkLeafActive($leaf, $parent);
			$children = null;
			if(isset($leaf['children'])){
				$children = $this->prepareMenu($leaf['children'], $leaf, $depth+1);
			}

			$aClass = [];
			if(isset($leaf['class'])){ $aClass[] = $leaf['class']; }
			if(isset($leaf['active'])){ $aClass[] = 'active'; }
			if($children){ $aClass[] = 'parent'; }

			$class = implode(' ', $aClass);
			$menu .= "<li class=\"{$class}\">";
			$menu .= $this->prepareItem($leaf);
			if($children){
				$menu .= $children;
			}
			$menu .= "</li>";
		}
		$menu .= '</ul>';
		return $menu;
	}

	/**
	 *
	 * @param array $item
	 * @return boolean
	 */
	protected function checkAcl($item){
		if(!isset($item['acl'])){
			return true;
		}
		if(App::$user->permits >= $item['acl']){
			return true;
		}
		return false;
	}

	/**
	 *
	 * @param array $leaf
	 * @param array $parent
	 */
	protected function checkLeafActive(&$leaf, &$parent){
		$c = lcfirst(Request::initial()->controller());
		$a = lcfirst(Request::initial()->action());
		if($leaf['link'] == $c){
			$leaf['active'] = true;
			if($parent){
				$parent['active'] = true;
			}
			return true;
		}
		if($leaf['link'] == "{$c}/{$a}"){
			$leaf['active'] = true;
			if($parent){
				$parent['active'] = true;
			}
			return true;
		}
		if(isset($leaf['active'])){
			if($parent){
				$parent['active'] = true;
			}
		}
	}

	/**
	 *
	 * @param array $item
	 * @return string
	 */
	protected function prepareItem($item){
		$link = strstr($item['link'], '#') ? Request::detect_uri().$item['link'] : $item['link'];
		return HTML::anchor($link, $item['title']);
	}

	/**
	 * Zwraca całe menu
	 * @return string
	 */
	public function get(){
		return $this->menuHtml;
	}

	/**
	 * Zwraca ścieżkę do szablonu z menu
	 * @param array $menuItem
	 * @return string
	 */
	public function getTemplatesPath($menuItem){
		return $this->menuConfig->templatesDirectory
				.(isset($menuItem['templatesSubPath']) ? $menuItem['templatesSubPath'] : '');
	}

	/**
	 * Zwraca element menu po podaniu linku
	 * @param string $link
	 * @return string|null
	 */
	public function getByLink($link){
		return array_key_exists($link, $this->menuFlatByUrl) ? $this->menuFlatByUrl[$link] : null;
	}

}
