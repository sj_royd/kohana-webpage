<?php defined('SYSPATH') or die('No direct script access.');

/**
 *
 */
class Kohana_Controller_Ajax extends Controller {

	protected $outputFormat = 'json';
	protected $outputData;

	/**
	 * Konstruktor
	 * @param \Request $request
	 * @param \Response $response
	 */
	public function __construct(\Request $request, \Response $response) {
		parent::__construct($request, $response);
	}

	/**
	 *
	 */
	public function after() {
		switch($this->outputFormat){
			case 'plain':
				$this->response->body($this->outputData);
				break;
			case 'json':
				$this->response->headers('Content-Type', 'application/json; charset=' . Kohana::$charset);
				$this->response->body(json_encode($this->outputData));
				break;
			case 'xml':

				break;
		}

		parent::after();
	}

	/**
	 * Tworzy odpowiedź
	 * @param mixed $data
	 * @param string $outputFormat
	 */
	public function response($data, $outputFormat = 'json'){
		$this->outputFormat = $outputFormat;
		$this->outputData = $data;
	}

	/**
	 *
	 * @param type $data
	 */
	public function addToResponse($data){
		$this->outputData['additional'] = $data;
	}

	/**
	 * Tworzy odpowiedź poprawną
	 * @param type $data
	 * @param type $outputFormat
	 */
	public function validResponse($data = null, $outputFormat = 'json'){
		$this->response(array(
			'success' => 1,
			'data' => $data
		), $outputFormat);
	}

	/**
	 * Tworzy odpowiedź niepoprawną
	 * @param type $data
	 * @param type $outputFormat
	 */
	public function invalidResponse($data = null, $outputFormat = 'json'){
		$this->response(array(
			'success' => 0,
			'data' => $data
		), $outputFormat);
	}

	/**
	 * Utrzymuje sesję użytkownika
	 */
	public function action_sessionKeepAlive(){
		Session::instance()->set('__keepAlive', time());
	}
}
