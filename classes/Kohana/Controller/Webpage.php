<?php

class Kohana_Controller_Webpage extends Controller_Template {

	/**
	 *
	 * @var Config_Group
	 */
	protected $config;

	private $lang = null;

	public function before() {

		$this->config = Kohana::$config->load('webpage');
		$this->checkMobile();
		parent::before();
		if($this->auto_render){
			$this->lang();
			if($this->config->get('strategy') == 'file'){
				$this->template->menu = $this->config->get('menu');
			}
		}
	}

	/**
	 *
	 */
	private function lang(){
		$langs = Kohana::$config->load('multilang')->get('languages');
		if(($lang = $this->request->param('lang'))){
			$this->lang = $lang;
			I18n::lang($langs[$lang]['i18n']);
			// Set locale
			setlocale(LC_ALL, $langs[$lang]['locale']);
		}
	}

	public function action_index(){

		$page = $this->request->param('page');

		$this->template->content = Model::factory('Page')->getContentByUrl(($this->lang?"{$this->lang}/":'').$page);

	}


	public function action_goToMobile(){
		$redirect = $this->config->get('redirect');
		if($redirect && Arr::get($redirect, 'mobile')){
			$this->redirect($this->config->get('redirect')['mobile']);
		}
	}

	public function action_goToDesktop(){
		$redirect = $this->config->get('redirect');
		if($redirect && Arr::get($redirect, 'desktop') && Arr::get($redirect, 'desktopDomain')){
			Cookie::$domain = Arr::get($redirect, 'desktopDomain');
			Cookie::set('showDesktop', 1);
			$this->redirect($this->config->get('redirect')['desktop']);
		}
	}


	private function checkMobile(){
		$redirect = $this->config->get('redirect');
		if($redirect && Arr::get($redirect, 'mobile') && $this->request->user_agent('mobile') && !Cookie::get('showDesktop')){
			$this->redirect($this->config->get('redirect')['mobile']);
		}
	}


	public function action_media(){
		 // Get the file path from the request
        $file = $this->request->param('filepath');

        // Find the file extension
        $ext = pathinfo($file, PATHINFO_EXTENSION);

        // Remove the extension from the filename
        $file = substr($file, 0, -(strlen($ext) + 1));

        // Find the file in 'media/' dirs across the cascading file system
        $filename = Kohana::find_file('media', $file, $ext);

        if ($filename) {
            // Get the file content and deliver it
            $this->response->body(file_get_contents($filename));

            // Set the proper headers to allow caching
            $this->response->headers('Content-Type', File::mime_by_ext($ext));
            $this->response->headers('Content-Length', filesize($filename));
            $this->response->headers('Last-Modified', date('r', filemtime($filename)));
        } else {
            $this->response->status(404);
            $this->response->body("{$file} brak");
            // Throw some suitable exception here
        }
	}


	public function action_sendMail(){

		$config = Kohana::$config->load('mail');

		$email = $this->request->post('email');
		$subject = $this->request->post('title');
		$messageBody = $this->request->post('content');
		$recipient = $config->get('autoRecipient');


		try {
			$mailer = Mailer::factory();
			$mailer->subject($subject);
			if(Valid::email($email)){
				$mailer->from($email);
				$mailer->reply_to($email);
			} else {
				$mailer->from($recipient);
				$messageBody .= "\r\nKontakt: ".$email;
			}
			$mailer->body(nl2br($messageBody));
//			$mailer->mailer->setLanguage('pl');
			$status = $mailer->send($config->get('autoRecipient'));

			if(!$status){
				throw new Exception($mailer->error());
			}

			$this->ajax->validResponse('Wiadomość została wysłana');
		} catch (Exception $ex) {
			$this->ajax->invalidResponse($ex->getMessage());
		}
	}

}
