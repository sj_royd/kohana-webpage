<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Abstrakcyjna klasa kontrolera szablonu
 */
abstract class Controller_Template extends Kohana_Controller_Template {

	/**
	 * @var Controller_Ajax
	 */
	protected $ajax;

	protected $authRequired = false;
	protected $minPermits;

	protected $topContent;
	protected $title;

	/**
	 * Kontroler szablonu
	 * @param \Request $request
	 * @param \Response $response
	 */
	public function __construct(\Request $request, \Response $response) {
		parent::__construct($request, $response);
//		Session::instance();

	}

	/**
	 * Inicjalilzuje stronę przed wyświetleniem
	 */
	public function before() {
		if($this->request->is_ajax()){
			$this->auto_render = false;
			$this->ajax = new Controller_Ajax($this->request, $this->response);
			$this->ajax->before();
		}
		parent::before();
	}

	/**
	 * Przypisuje podwidoki do szabonu głównego strony
	 */
	public function after(){
		if ($this->auto_render){

		} else {
			$this->ajax->after();
			$this->response = $this->ajax->response;
		}

		parent::after();
	}

}
